const FuzzySet = require('fuzzyset.js');
const fuzzysort = require('fuzzysort');
const Fuse = require('fuse.js');
const {filter} = require('fuzzaldrin');
const fuzzy = require('fuzzy');
const fuzzysearch = require('fuzzysearch');

const data = [
  'Office_A',
  'Office_B',
  'Office_C',
  'Office_1',
];

const tests = ['office c', 'office one'];

const performance = (label, fn) => {
  console.log(`==== ${label} ====`);
  const start = Date.now();
  fn();
  console.log(`[${label}] spent: ${ Date.now() - start }ms`);
  console.log('======================');
  console.log('');
};

const testFuzzySet = () => {
  const deviceNames = FuzzySet(data);
  const toString = result => ` { ${result[1]}, socre: ${result[0]} }`;
  tests.forEach(test => console.log(`input ${test} ==> ${deviceNames.get(test).map(toString)}`));
};
performance('FuzzySet', testFuzzySet);

const testFuzzySort = () => {
  const toString = result => `${result.target}, socre:${result.score}`;
  tests.forEach(test => console.log(`input ${test} ==> ${fuzzysort.go(test, data).map(toString)}`));  
};
performance('FuzzySort', testFuzzySort);

const testFuseJs = () => {
  const options = {
    shouldSort: true,
    includeScore: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
      'name',
    ],
  };
  const deviceNames = new Fuse(data.map(name => {return {name};}), options);
  const toString = result => ` { ${result.item.name}, socre: ${result.score} }`;
  tests.forEach(test => console.log(`input ${test} ==> ${deviceNames.search(test).map(toString)}`));
};
performance('Fuse', testFuseJs);

const testFuzzaldrin = () => {
  const toString = result => `${result}`;
  tests.forEach(test => console.log(`input ${test} ==> ${filter(data, test).map(toString)}`));  
};
performance('Fuzzaldrin', testFuzzaldrin);

const testFuzzy = () => {
  const toString = result => `{ ${result.string}, socre: ${result.score} }`;
  tests.forEach(test => console.log(`input ${test} ==> ${fuzzy.filter(test, data).map(toString)}`));  
};
performance('fuzzy', testFuzzy);

const testFuzzysearch = () => {
  tests.forEach(test => {
    const results = [];
    data.forEach(d => {
      if (fuzzysearch(test, d)) {
        results.push(d);
      }
    });
    console.log(`input ${test} ==> ${results}`);
  });  
};
performance('fuzzysearch', testFuzzysearch);
